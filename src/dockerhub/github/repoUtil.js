const randomstring = require('randomstring')
const axios = require('axios')
const { execSync, exec } = require('child_process')
const { GITHUB_ACCESS_TOKEN, GITHUB_USERNAME } = require('../../config/secret')
const db = require('../../db/connect')
const dayjs = require('dayjs')
const logger = require('../../log/logger')

const namePrefix = randomstring.generate({ length: 5, charset: 'alphabetic', capitalization: 'lowercase' })
const MAX_REPO_NUM = 50

const createRepo = async (namePrefix) => {
  await Promise.all(
    Array.from({ length: MAX_REPO_NUM }).map((v, index) => {
      const reponame = `${namePrefix}${index}`
      return new Promise((resolve) => {
        axios({
          method: 'POST',
          url: 'https://api.github.com/user/repos',
          headers: {
            Authorization: `token ${GITHUB_ACCESS_TOKEN}`
          },
          data: {
            name: reponame,
            private: true
          }
        })
          .then(async () => {
            await db.insert('repo', {
              repo_name: reponame,
              username: GITHUB_USERNAME
            })
            resolve()
          })
          .catch((err) => {
            console.log(err)
          })
      })
    })
  )
}

const deleteRepo = async () => {
  const [repos] = await db.query('select * from repo where bind_user_num != 0')
  await Promise.all(
    repos.slice(0, MAX_REPO_NUM).map((item) => {
      return new Promise((resolve) => {
        axios({
          method: 'DELETE',
          url: `https://api.github.com/repos/${item.username}/${item.repo_name}`,
          headers: {
            Authorization: `token ${GITHUB_ACCESS_TOKEN}`
          }
        })
          .then(() => {
            db.query('delete from repo where id = ?', [item.id])
            execSync(`git remote remove ${item.repo_name}`)
            console.log(`${item.repo_name}删除成功`)
            resolve()
          })
          .catch((err) => {
            if (err.message == 'Request failed with status code 404') {
              db.query('delete from repo where id = ?', [item.id])
            }
            // execSync(`git remote remove ${item.repo_name}`)
            logger.error(err.stack)
          })
      })
    })
  )
}

const pushCode = async () => {
  const [repos] = await db.query('select * from repo')
  const date = dayjs().format('YYYY-MM-DD_HH:mm:ss')
  execSync(`echo ${date} >> temp.md && git add . && git commit -m ${date}`)

  await Promise.all(
    repos.map((v) => {
      return new Promise((resolve) => {
        exec(`git push ${v.repo_name} master -f`, (err) => {
          if (!err) {
            logger.text(`提交代码成功，${v.repo_name}`)
            resolve()
          } else {
            logger.error(err.stack)
            try {
              execSync(
                `git remote add ${v.repo_name} https://oauth2:${GITHUB_ACCESS_TOKEN}@github.com/${GITHUB_USERNAME}/${v.repo_name}.git`
              )
              execSync(`git push ${v.repo_name} master -f`)
            } catch (error) {
              logger.error(err.stack)
            }
            resolve()
          }
        })
      })
    })
  )
}

module.exports = { namePrefix, createRepo, deleteRepo, pushCode, MAX_REPO_NUM }
